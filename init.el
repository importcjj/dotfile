

;;(eval-when-compile
;;  (require 'use-package))


(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-faces-vector
   [default default default italic underline success warning error])
 '(column-number-mode t)
 '(custom-enabled-themes '(misterioso))
 '(display-battery-mode t)
 '(display-time-mode t)
 '(fringe-mode 0 nil (fringe))
 '(global-display-line-numbers-mode t)
 '(package-selected-packages
   '(lsp-python-ms smex yasnippet-snippets markdown-mode+ ace-jump-mode counsel autopair company-lsp use-package lsp-mode))
 '(scroll-bar-mode nil)
 '(show-paren-mode t)
 '(size-indication-mode t)
 '(tool-bar-mode nil))
(global-visual-line-mode 1)
(global-display-line-numbers-mode 1)
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:family "Consolas" :foundry "outline" :slant normal :weight normal :height 120 :width normal)))))


(require 'package)
(add-to-list 'package-archives
	     '("melpa-stable" . "https://stable.melpa.org/packages/")) (package-initialize)

(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

(use-package bookmark+
  :load-path "manual/bookmark-plus")

;; M-x package-install markdown-mode
;; markdown preview cheetsheet
;; C-c C-c p preivew with browser
;; C-c C-c e export to specificed file
;; C-c C-c l preview with eww buffer
(use-package markdown-mode
  :ensure t
  :commands (markdown-mode gfm-mode)
  :mode (("README\\.md\\'" . gfm-mode)
	 ("\\.md\\'" . markdown-mode)
	 ("\\.markdown\\'" .  markdown-mode))
  :init (setq markdown-command "multimarkdown"))


;; M-x package-install ivy
(use-package ivy
  :ensure t
  :config
  (ivy-mode 1)
  (setq ivy-use-virtual-buffers t)
  (setq ivy-count-format "(%d|%d) ")
  :init
  (bind-key "C-s" 'swiper-isearch)
  (bind-key "M-x" 'counsel-M-x)
  (bind-key "C-x C-f" 'counsel-find-file)
  (bind-key "M-y" 'counsel-yank-pop)
  (bind-key "C-x b" 'ivy-switch-buffer)
  (bind-key "C-c g" 'counsel-git)
  (bind-key "C-c L" 'counsel-git-log)
  (bind-key "C-c b" 'counsel-bookmark))


;; M-x package-install ace-jump-mode
(use-package ace-jump-mode
  :commands ace-jump-mode
  :init
  (bind-key "C-c f" 'ace-jump-mode))

;; M-x package-install yasnippet-snippets
(use-package yasnippet-snippets
  :ensure t
  :commands yas-minor-mode
  :hook (lsp-mode . yas-minor-mode))

;; M-x package-install autopair
;; Auto close the bracket
;; Mode actived in specificed modes
(use-package autopair)
 (defun autopair-mode-active ()
  (autopair-mode 1))


(add-hook 'go-mode-hook #'autopair-mode-active)
(add-hook 'rust-mode-hook #'autopair-mode-active)

;; M-x package-install lsp-mode
(use-package lsp-mode
  :ensure t
  :commands (lsp lsp-deferred)
  :hook
  (autopair-mode . lsp-deferred)
  (go-mode . lsp-deferred)
  (python-mode . lsp-deferred)
  (rust-mode . lsp-deferred))

(use-package lsp-ui
  :commands lsp-ui-mode)

(defun lsp-go-install-save-hooks ()
  (display-line-numbers-mode 1)
  (add-hook 'before-save-hook #'lsp-format-buffer t t)
  (add-hook 'before-save-hook #'lsp-organize-imports t t))
(add-hook 'go-mode-hook #'lsp-go-install-save-hooks)

;; M-x package-install company-lsp
(use-package company-lsp
  :ensure t
  :config (progn
	    (setq company-idle-delay 0)
	    (setq company-minimu-prefix-length 1)
	    (setq company-tooltip-align-annotations t))
  :commands company-lsp)

;; M-x package-install go-mode
(use-package go-mode
  :defer t
  :ensure t)

;; M-x package-install rust-mode
(use-package rust-mode
  :defer t
  :config
  (setq lsp-rust-server 'rust-analyzer)
  :ensure t)

(add-hook 'before-save-hook (lambda () (when (eq 'rust-mode major-mode)
					   (lsp-format-buffer))))

;;(use-package lsp-python-ms
;;  :ensure t
;;  :init (setq lsp-python-ms-auto-install-server t)
;;  :hook (python-mode . (lambda ()
;;			 (require 'lsp-python-ms)
;;			 (lsp-deferred))))
